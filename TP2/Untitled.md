TP2:

I. Création et utilistation simples d'une VM CentOS

4. Configuration réseau d'une machine CentOS

Liste des cartes réseau

|  Name   | IP | MAC | Foncion |
| --- | -------- | -------- | -------- |
|  lo   |     127.0.0.1     |    1/128      |     Le loopback c'est la possibilité, depuis son réseau interne, d'accéder à une ressource du LAN en utilisant l’IP WAN externe du routeur ou son DNS associé     |
|  enp0s3   |    10.0.2.15      |     fe80:fb19 :3cd0 :820e :162     |     se connecter et accéder à internet     |
|  enp0s8   | 10.2.1.2    | Fe80:a00:27ff:fec7:1027   | Host only     |

Configuration de la carte réseau Host-Only

![](https://i.imgur.com/pTNazii.png)

![](https://i.imgur.com/9OhZS2K.png)

5. Appréhension de quelques commandes

Scan nmap

![](https://i.imgur.com/OKJkqxx.png)

Utilisation de "ss"

![](https://i.imgur.com/lbE8kBj.png)

II. Notion de ports

1. SSH

IP et ports sur lequelles le serveur SSH écoute

![reference link](https://i.imgur.com/skYB5eB.png)

Effectuer une connexion SSH

![](https://i.imgur.com/RNmwcJg.png)

A.SSH

Modifier le port du service SSH 

![](https://i.imgur.com/AyhTg3Q.png)

Vérification que le service SSH écoute sur le nouveau port choisi:
![](https://i.imgur.com/W9hamFP.png)

Le firewall bloque la connexion
![](https://i.imgur.com/tWi9B2U.png)

Après avoir rentré la commande: sudo firewall-cmd --add-port=80/tcp --permanent
Le firewall ne bloque plus la connexion
![](https://i.imgur.com/2ucfKNI.png)

B.Netcat

![](https://i.imgur.com/T2MP9Jj.png)
```
PS C:\Users\theoz\Desktop\netcat-win32-1.12> .\nc.exe 10.2.1.78 7003
_çàç_
ty
```

3.Wireshark 

Mise en évidence que les messages envoyés avec netcat sont visibles dans Wireshark

![](https://i.imgur.com/eyyxSWj.png)


III. Routage statique

PC1 ping PC2: 
````
Envoi d’une requête 'Ping'  10.2.12.2 avec 32 octets de données :
Réponse de 10.2.12.2 : octets=32 temps=6 ms TTL=128
Réponse de 10.2.12.2 : octets=32 temps=2 ms TTL=128

Statistiques Ping pour 10.2.12.2:
    Paquets : envoyés = 2, reçus = 2, perdus = 0 (perte 0%),
Réponse de 10.2.12.2 : Durée approximative des boucles en millisecondes :
    Minimum = 2ms, Maximum = 6ms, Moyenne = 4ms
````
PC2 ping PC1 (disponible sur le TP de Benjamin Chancerel)

````
PS C:\Windows\system32> ping 10.2.12.2

Envoi d’une requête 'Ping'  10.2.12.2 avec 32 octets de données :
Réponse de 10.2.12.2 : octets=32 temps=2 ms TTL=128
Réponse de 10.2.12.2 : octets=32 temps=2 ms TTL=128
Réponse de 10.2.12.2 : octets=32 temps=2 ms TTL=128
Réponse de 10.2.12.2 : octets=32 temps=3 ms TTL=128

Statistiques Ping pour 10.2.12.2:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 2ms, Maximum = 3ms, Moyenne = 2ms
````